import math
import os
import argparse

import numpy as np
import pandas as pd
from aeroclient.sherlock import get_sherlock_drf_client
from aerogeom.point import Point, LabeledPointCollection
from aerogeos.constants import COORDS_LONLAT
from aeroio.io import join
from aeroutil.log import create_logger
from mapenginelib.models import Survey
from treeenglib.fetch import get_survey_imagery

ZERO_CELSIUS_IN_KELVIN = 273.15
GSD_CM = 10
logger = create_logger('orchard-visualisations')

CSV_SURVEY_ID_COLUMN_NAME = "survey_id"
CSV_CROP_NAME_COLUMN_NAME = "croptype_name"


def apply_pch_cut(trees_df):
    transp_1_99_list = []
    feature_name = "transpiration"
    transp_min = np.nanpercentile(trees_df[feature_name].values, 1)
    transp_max = np.nanpercentile(trees_df[feature_name].values, 99)

    for i, row in trees_df.iterrows():
        transpiration = row.get("transpiration")
        if transpiration <= transp_min:
            transp_1_99 = transp_min
        elif transpiration > transp_max:
            transp_1_99 = transp_max
        else:
            transp_1_99 = transpiration

        transp_1_99_list.append(transp_1_99)

    trees_df["transpiration_1_99"] = transp_1_99_list

    return trees_df


def get_survey_ids_from_csv(csv_name, survey_id_col_name=CSV_SURVEY_ID_COLUMN_NAME):
    survey_df = pd.read_csv(csv_name, index_col=False)
    # survey_df = survey_df[survey_df["status_id"] == 1]
    return list(survey_df[survey_id_col_name].values)


def get_tree_data(survey_id, trees_data):
    trees_api = get_sherlock_drf_client('trees')
    treesurveys_response = trees_api.treesurvey_list(survey_id=survey_id)
    treesurveys = treesurveys_response["response"]
    trees_df = pd.DataFrame(treesurveys)

    if "transpiration" not in list(trees_df.columns):
        logger.error(f"Skipping {survey_id} since no transpiration data available")
        return None
    elif trees_df["transpiration"].isna().any():
        na_count = np.count_nonzero(trees_df["transpiration"].isna())
        logger.error(f"Skipping {survey_id} since transpiration has {na_count} nan values")
        return None
    else:
        trees_df.to_csv(trees_data, index=False)

    return trees_df


def get_survey_df(survey_id, data_dir, use_cached_tree_data=True):
    trees_data = join(data_dir, f"{survey_id}_data.csv")

    if use_cached_tree_data:
        if os.path.exists(trees_data):
            trees_df = pd.read_csv(trees_data, index_col=False)
        else:
            logger.info(f"No cached data found for {survey_id}. Using trees API")
            trees_df = get_tree_data(survey_id, trees_data)
    else:
        trees_df = get_tree_data(survey_id, trees_data)

    return trees_df


def create_farm_view_per_crop(arguments):

    working_dir = arguments.working_dir
    os.makedirs(working_dir, exist_ok=True)
    farm_name = arguments.farm_name

    farm_crop_data = arguments.input_file_name
    data_df = pd.read_csv(farm_crop_data, index_col=False)

    survey_id_col = CSV_SURVEY_ID_COLUMN_NAME
    crop_name_col = CSV_CROP_NAME_COLUMN_NAME

    survey_ids = list(data_df[survey_id_col].values)
    logger.info(survey_ids)

    crop_names = list(data_df[crop_name_col].unique())
    for crop_name in crop_names:

        if crop_name == "Table Grapes":
            logger.error(f"Cannot create irrigation view for table grapes, skipping crop type")
            continue
        else:
            df_farm = data_df[data_df[crop_name_col] == crop_name]
            survey_ids_per_crop = list(df_farm[survey_id_col].values)
            logger.info(f"{crop_name}: {survey_ids_per_crop}")
            crop_farm_name = farm_name + "_" + crop_name
            create_full_farm_view(working_dir, crop_farm_name, survey_ids_per_crop, use_cached_tree_data=True)


def download_survey_imagery(working_dir, survey_ids):
    # Download all survey visible images
    logger.info("Downloading visible images for surveys")
    for survey_id in survey_ids:
        survey_dir = join(working_dir, f"{survey_id}")
        os.makedirs(survey_dir, exist_ok=True)
        survey = Survey.from_id(survey_id)

        download_dir = join(survey_dir, f"downloads")
        os.makedirs(download_dir, exist_ok=True)

        # Visible Image - visiible.tif
        file_name = join(download_dir, f"visible_{GSD_CM}cm.tif")
        if not os.path.exists(file_name):
            logger.info(f"Downloading .tif files for {survey_id}")
            img_type_to_local_path = get_survey_imagery(survey.id, ["visible"], download_dir,
                                                        fixed_gsd_cm_per_px=GSD_CM)
            if "visible" in img_type_to_local_path.keys():
                logger.info(f"Successfully downloaded {img_type_to_local_path['visible']}")
        else:
            logger.info(f"Visible_{GSD_CM}cm.tif is already available for {survey_id}")
            continue


def create_farm_view(arguments):
    working_dir = arguments.working_dir
    os.makedirs(working_dir, exist_ok=True)

    farm_name = arguments.farm_name
    input_csv_file = f"input_csv_data/{arguments.input_file_name}"

    survey_ids = get_survey_ids_from_csv(input_csv_file, survey_id_col_name=CSV_SURVEY_ID_COLUMN_NAME)
    print(survey_ids)

    create_full_farm_view(working_dir, farm_name, survey_ids)
    logger.info(f"Survey ID's = {survey_ids}")


def create_output_geojson(qgis_geojson_path, farm_df):
    tree_surveys = farm_df.to_dict("records")

    points = []
    label_data = {
        "radius": [],
        "transpiration": [],
        "transpiration_1_99": []
    }

    for ts in tree_surveys:
        point = Point((ts["lng"], ts["lat"]), COORDS_LONLAT)
        points.append(point)
        radius = (math.sqrt(ts['area'] / math.pi)) * 2
        label_data["radius"].append(radius)
        label_data["transpiration"].append(ts["transpiration"])
        label_data["transpiration_1_99"].append(ts["transpiration_1_99"])

    point_collection = LabeledPointCollection(points)
    point_collection.add_continuous_label_with_values('radius', label_data["radius"])
    point_collection.add_continuous_label_with_values('transpiration', label_data["transpiration"])
    point_collection.add_continuous_label_with_values('transpiration_1_99', label_data["transpiration_1_99"])
    point_collection.write_to_geojson(qgis_geojson_path)


def create_full_farm_view(working_dir, farm_name, survey_ids, use_cached_tree_data=True):

    logger.info(f"Creating farm view for : {farm_name} with Survey_ids = {survey_ids}")

    # Download all the survey tif files
    download_survey_imagery(working_dir, survey_ids)

    output_dir = "output_dir"
    os.makedirs(output_dir, exist_ok=True)

    full_farm_dir = join(output_dir, farm_name)
    os.makedirs(full_farm_dir, exist_ok=True)
    full_farm_csv = join(full_farm_dir, f"{farm_name}.csv")

    if os.path.exists(full_farm_csv):
        full_farm_df = pd.read_csv(full_farm_csv, index_col=False)
    else:
        full_farm_df = pd.DataFrame()
        data_dir = join(full_farm_dir, "data")
        os.makedirs(data_dir, exist_ok=True)

        # Combine the dataframes for all these surveys
        for survey_id in survey_ids:
            survey_df = get_survey_df(survey_id, data_dir, use_cached_tree_data)

            if survey_df is None:
                logger.error(f"{survey_id} failed to fetch trees data")
                continue

            logger.info(f"Done trees api call for {survey_id}")
            full_farm_df = full_farm_df.append(survey_df, ignore_index=True)

        if full_farm_df.empty:
            logger.error('full_farm_df DataFrame is empty!')
            return

        # Normalise according too same procedure using min/max and
        full_farm_df['full_median_transpiration'] = np.nanmedian(full_farm_df['transpiration'].values)
        full_farm_df['full_relative_transpiration'] = full_farm_df['transpiration'] - full_farm_df['full_median_transpiration']
        full_farm_df = apply_pch_cut(full_farm_df)
        # Save to csv
        full_farm_df.to_csv(full_farm_csv, index=False)

    logger.info("Starting qgis work")
    qgis_geojson_path = join(full_farm_dir, f"qgis_{farm_name}.geojson")
    create_output_geojson(qgis_geojson_path, full_farm_df)
    logger.info(f"Done with qgis point collection for farm {farm_name}")


def create_relative_survey_full_farm_view(working_dir, farm_name, survey_ids, download_visible=True):

    # Download all survey visible images
    if download_visible:
        logger.info("Downloading visible images for surveys")
        for survey_id in survey_ids:
            survey_dir = join(working_dir, f"{survey_id}")
            os.makedirs(survey_dir, exist_ok=True)
            survey = Survey.from_id(survey_id)

            download_dir = join(survey_dir, f"downloads")
            os.makedirs(download_dir, exist_ok=True)

            file_name = join(download_dir, f"visible_{GSD_CM}cm.tif")
            if not os.path.exists(file_name):
                logger.info("Downloading .tif files")
                img_type_to_local_path = get_survey_imagery(survey.id, ["visible"], download_dir,
                                                            fixed_gsd_cm_per_px=GSD_CM)
                if "visible" in img_type_to_local_path.keys():
                    logger.info(f"Successfully downloaded {img_type_to_local_path['visible']}")
            else:
                logger.info(f"Visible_{GSD_CM}cm.tif is already available for {survey_id}")
                continue

    output_dir = "output_dir"
    os.makedirs(output_dir, exist_ok=True)

    full_farm_dir = join(output_dir, farm_name)
    os.makedirs(full_farm_dir, exist_ok=True)

    data_dir = join(full_farm_dir, "data")
    os.makedirs(data_dir, exist_ok=True)

    # Combine the dataframes for all these surveys
    for survey_id in survey_ids:
        survey_data = join(data_dir, f"{survey_id}_data.csv")

        if os.path.exists(survey_data):
            survey_df = pd.read_csv(survey_data, index_col=False)
        else:
            logger.info(f"No cached data found for {survey_id}. Using trees API")

            trees_api = get_sherlock_drf_client('trees')
            treesurveys_response = trees_api.treesurvey_list(survey_id=survey_id)
            treesurveys = treesurveys_response["response"]
            survey_df = pd.DataFrame(treesurveys)

            if survey_df is None or "transpiration" not in list(survey_df.columns):
                logger.error(f"{survey_id} failed to fecth trees data")
                continue

            # Now apply pch_1_99 cut to each survey
            survey_df['full_median_transpiration'] = np.nanmedian(survey_df['transpiration'].values)
            survey_df['full_relative_transpiration'] = survey_df['transpiration'] - survey_df[
                'full_median_transpiration']
            survey_df = apply_pch_cut(survey_df)
            survey_df.to_csv(survey_data, index=False)

        logger.info(f"Finished trees api call for {survey_id}")
        # full_farm_df = full_farm_df.append(survey_df, ignore_index=True)
        # Create a geojson for each file
        survey_dir = join(working_dir, f"{survey_id}")
        os.makedirs(survey_dir, exist_ok=True)

        download_dir = join(survey_dir, f"downloads")
        os.makedirs(download_dir, exist_ok=True)

        survey_geojson = join(download_dir, f"{survey_id}.geojson")
        create_output_geojson(survey_geojson, survey_df)
        logger.info(f"Done with qgis point collection for {survey_id}")


def create_per_survey_full_farm_view(arguments):
    working_dir = arguments.working_dir
    os.makedirs(working_dir, exist_ok=True)

    input_csv_file = arguments.input_file_name
    survey_ids = get_survey_ids_from_csv(input_csv_file, survey_id_col_name=CSV_SURVEY_ID_COLUMN_NAME)
    logger.info(survey_ids)

    farm_name = arguments.farm_name
    create_relative_survey_full_farm_view(
        working_dir, farm_name, survey_ids, download_visible=arguments.download_visible_files
    )


if __name__ == "__main__":

    # Parse command line arguments
    parser = argparse.ArgumentParser(description='Full farm view parser')
    parser.add_argument('--farm-view-type',
                        type=str,
                        required=True,
                        metavar="farm view type",
                        help="Type of full farm view to create")
    parser.add_argument('--working-dir',
                        type=str,
                        required=False,
                        default="working_dir",
                        metavar=" working directory",
                        help="The directory to store survey data in")
    parser.add_argument('--farm-name',
                        type=str,
                        required=True,
                        default="Farm 1",
                        metavar="farm name",
                        help="The name of the farm")
    parser.add_argument('--input-file-name',
                        type=str,
                        required=True,
                        default="working_dir",
                        metavar="input_file_name",
                        help="The input file name with survey data inside")
    parser.add_argument('--download-visible-files',
                        type=str,
                        required=False,
                        default="True",
                        metavar="farm name",
                        help="Determine whether to download visible.tif files from s3")

    args = parser.parse_args()
    logger.info(f"Script arguments = {str(args)}.")

    if len(str(args)) < 2:
        logger.info("Missing arguments")
        exit()

    print(args)

    if args.farm_view_type == "farm_view":
        create_farm_view(args)
    elif args.farm_view_type == "farm_view_per_crop":
        create_farm_view_per_crop(args)
    elif args.farm_view_type == "farm_view_per_survey":
        create_per_survey_full_farm_view(args)
