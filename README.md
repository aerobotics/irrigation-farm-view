
### What is this repository for? ###

Creating full farm views:
* Farm View
* Farm View per survey (Do not aggregate transpiration)
* Farm View per crop type

### How do I get set up? ###
Dependencies:
- Python 3
- Pip
- Aeroless

Using pip we install the required dev dependencies using `pip install -r requirements.txt`

Then log into Aeroless using `aeroless login`

### How do I find the correct survey csv data
We use dbeaver and SQL to find the correct survey data. Given the farm_id and date range for surveys, use the 
following query to find the correct surveys.

```
select
	distinct s.id,
	s.status_id,
	s."date",
	s.orchard_id,
	s2.layer_id
from
	public.survey s
join public.orchard o on
	o.id = s.orchard_id
join public.surveylayers s2 on
	s2.survey_id = s.id
where
	s."date" >= '2021-06-17'
	and o.farm_id = 8856
	and s2.layer_id = 27
order by
	id
```


### Folder Structures
Inside the repository, any input survey data must be placed in the *input_csv_data* folder. Inside there, place any comma separated values (.csv) file. An example of the structure of the csv is as follows:

```
survey_id,croptype_id,croptype_name
66863,4,Peach
65692,4,Peach
65872,66,Nectarine
65804,66,Nectarine
65878,66,Nectarine
```

Note that the column headers are:
- **survey_id (Required)**
- croptype_id (Optional)
- croptype_name (Optional)


### How do I run the script
`
python full_farm_views.py --farm-view-type farm_view --farm-name HMC --input-file-name example.csv
`


## Steps to produce a full farm view
The main steps are:
1. Run the python script "full_farm_views.py"
   - Run the correct command in the terminal
   
2. Pull script outputs into QGIS
   - Open the "qgis_[farm_name].geojson" file in QGIS
   - Open a python console in QGIS
     - Find the "Plugins" button in the top toolbar, then open "Python Console")
     - Click the "show editor button in the Python Console"
     - Click the "Open Script" button and navigate to "qgis_script.py" in the irrigation-farm-view repository
   - Once loaded, add the survey ID's of all the surveys we are looking at
     - These can be found in the end of the terminal where the python script was run
   - Once the survey IDs are present, run the script to import all the visible.tif files
   - Check that all .tif files have been correctly imported on the layers tab
     - If any have issues importing, a notification will appear in the Python Console for each survey
   - Ensure the geojson file is on top of all the visible layers
   - Import the moisture_style.qml style file for the geojson layer

3. Edit the Geojson file
   - Now we need to edit the dots to be on the correct proportion and colour scale
   - Right-Click on the geojson layer in the Layers tab. Navigate to Properties
   - Navigate to *Symbology*
   - Click the dropdown box at the top of the Symbology Tab
   - Select _Single Symbol_
   - Change the Units to "Meters at Scale"
   - On the Size unit, click the drop-down on the right of the size category, then navigate to the field level and select radius  
   - Click _Apply_
   - Click the dropdown box at the top of the Symbology Tab
   - Select _Graduate Symbol_
   - Change the colour ramp to **Moisture**
   - Change the Mode to **Equal Interval**
   - Change **Classes** to 10
   - Then only, change **Symbol** to _"transpiration_1_99"_
   - **Click the Classify Button**
   - Click Okay

Now we should have the correct per tree values on the visible images. Ensure that all the per tree dots can be seen.
If everything looks correct - time to save.

### Saving the .png file
- Click on **Project** in the main toolbar
- Navigate to **import/export**
- Export Map to Image
- Set Resolution to **800** dpi
- Click Save and select where you want to save the file
