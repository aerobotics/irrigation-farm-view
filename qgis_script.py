import os
import processing
from qgis.core import QgsVectorLayer
from PyQt5.QtGui import QColor

survey_ids = [66687, 66688, 66689, 66690, 66691, 66692]

for survey_id in survey_ids:
    path = f"/home/ryan/Documents/aerobotics/irrigation-farm-view/working_dir/{survey_id}/downloads/visible_10cm.tif"
    if os.path.exists(path):
        iface.addRasterLayer(path, f"{survey_id}")
    else:
        print(f"Failed to load in visible tif image for {survey_id}")


def apply_per_tree_dot_size(geojson_layer):
    props = geojson_layer.renderer().symbol().symbolLayer(0).properties()
    # TODO: Cannot set the size variable here to a list of floats
    props['size'] = "wsi_1_99"
    # props['size'] = QgsVectorLayerUtils.getValues(geojson_layer, "wsi_1_99")
    props['size_unit'] = "RenderMetersInMapUnits"
    props['offset_unit'] = "RenderMetersInMapUnits"
    props['outline_width_unit'] = "RenderMetersInMapUnits"
    geojson_layer.renderer().setSymbol(QgsMarkerSymbol.createSimple(props))
    # show the changes
    geojson_layer.triggerRepaint()


def apply_colour_ramp(geojson_layer):
    color_ramp = QgsGradientColorRamp()

    colour_ramp_attributes = {
        "color1": QColor(0, 0, 51),
        "color2": QColor(204, 51, 0),
        "stops_and_colors": [
            QgsGradientStop(0.11, QColor(0, 0, 178)),
            QgsGradientStop(0.22, QColor(0, 102, 255)),
            QgsGradientStop(0.33, QColor(124, 204, 255)),
            QgsGradientStop(0.44, QColor(153, 204, 153)),
            QgsGradientStop(0.55, QColor(255, 255, 204)),
            QgsGradientStop(0.66, QColor(255, 204, 102)),
            QgsGradientStop(0.77, QColor(255, 127, 51)),
            QgsGradientStop(0.88, QColor(204, 102, 0)),
        ]
    }
    # Set Min and Max Colours
    color_ramp.setColor1(colour_ramp_attributes["color1"])
    color_ramp.setColor2(colour_ramp_attributes["color2"])
    # Set middle colours
    color_ramp.setStops(colour_ramp_attributes["stops_and_colors"])

    ramp_min = geojson_layer.minimumValue(geojson_layer.fields().indexFromName("wsi_1_99"))
    ramp_max = geojson_layer.maximumValue(geojson_layer.fields().indexFromName("wsi_1_99"))

    num_of_classes = 11

    intervals = QgsClassificationEqualInterval().classes(ramp_min, ramp_max, num_of_classes)

    render_range_list = [
        QgsRendererRange(i, QgsSymbol.defaultSymbol(geojson_layer.geometryType()))
        for i in intervals
    ]
    renderer = QgsGraduatedSymbolRenderer("wsi_1_99", render_range_list)
    renderer.updateColorRamp(color_ramp)
    geojson_layer.setRenderer(renderer)
